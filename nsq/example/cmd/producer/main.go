package main

import (
	"fmt"
	"log"
	"time"

	"github.com/nsqio/go-nsq"
)

// nsqd address and 4150 portss
const NsqdAddr = "172.21.0.4:4150"

func main() {
	config := nsq.NewConfig()
	producer, err := nsq.NewProducer(NsqdAddr, config)
	if err != nil {
		log.Fatal(err)
	}

	topicName := "order"

	for i := 0; i < 9; i++ {
		messageBody := []byte(fmt.Sprintf("Hello %d", i))
		err = producer.Publish(topicName, messageBody)
		if err != nil {
			log.Fatal(err)
		}

		time.Sleep(time.Second * time.Duration(1))
	}

	producer.Stop()
}
