# FAQ
1. Exit with code 78
```bash
ERROR: [1] bootstrap checks failed. You must address the points described in the following [1] lines before starting Elasticsearch.
bootstrap check failure [1] of [1]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]
```

Should to reset the vm.max_map_count
```bash
sudo sysctl -w vm.max_map_count=262144
```

2. FATAL  Error: Unable to complete saved object migrations for the [.kibana_task_manager] index. Please check the health of your Elasticsearch cluster and try again. Error: [cluster_block_exception]: index [.kibana_task_manager_7.12.0_001] blocked by: [TOO_MANY_REQUESTS/12/disk usage exceeded flood-stage watermark, index has read-only-allow-delete block]

```bash
curl -XPUT -H "Content-Type: application/json" http://localhost:9200/_all/_settings -d '{"index.blocks.read_only_allow_delete": null}'
```

### Export logstash to es
```bash
docker run --network es7_12_es7net --rm -it -v /home/kevin/docker-demo/es7_12/logstash/logstash.conf:/usr/share/logstash/pipeline/logstash.conf -v /home/kevin/docker-demo/es7_12/logstash/logstash.yml:/usr/share/logstash/config/logstash.yml -v /home/kevin/docker-demo/es7_12/logstash:/home/kevin/docker-demo/es7_12/logstash logstash:7.12.0
```

### Security
exception: Security must be explicitly enabled when using a [basic] license. Enable security by setting [xpack.security.enabled] to [true] in the elasticsearch.yml file and restart the node.

https://medium.com/@mandeep_m91/setting-up-elasticsearch-and-kibana-on-docker-with-x-pack-security-enabled-6875b63902e6


