package main

import (
	"fmt"

	"github.com/sirupsen/logrus"
)

func init() {
	logrus.Println("init server")
}

func main() {
	fmt.Println("service start")
}
